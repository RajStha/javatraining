
class IdentityTest {

	public static void main(String[] args) {
		Event e1 = new Event();
		Event e2 = new Event();
		if(e1==e2) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}
		
		Event e3 = e2;
		if (e3==e2){
			System.out.println("True");
		}
		
		
//		Calculator calc;
//		if(calc == null) {
//			System.out.println("True");
//		}
// no new object is created i.e. calc is null or it doesn't point to an object
		
		Calculator calc = null;
		if(calc == null) {
			System.out.println("True");
		}		
		
		String s1 = "abc";
		String s2 = "abc";
// only one object is created first by s1, while s2 just uses it.
		if (s1 == s2) {
			System.out.println("Same string object");
		}else {
			System.out.println("Individual String Objects");
		}
		
		String s3 = new String("abc");
		if (s1==s3) {
			System.out.println("S3 and S1 same object.");
		} else {
			System.out.println("S3 and S1 are different object");
		}
		if (s1.equals(s3)){
			System.out.println("Equal Strings");
		} else {
			System.out.println("Different Strings");
		}
	}
}
