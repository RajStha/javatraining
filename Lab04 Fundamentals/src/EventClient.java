
class EventClient {

	public static void main(String[] args) {
		Event evt = new Event();
		evt.method1();
		evt.method2(1);
		int result = evt.method3(2, 3);
		System.out.println("The returned value is: " + result);
	}
}
