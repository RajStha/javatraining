import java.util.*;
public class Square extends Rectangle{

//Calling Constructor of the parent class using new child class constructor.
	Square(int side){
		super(side, side);
	}

//Calling Constructor of the parent class using new child class constructor method2	
	Square(int length, int breadth) {
		super(length, breadth);	
	}

	public static void main(String[] args) {
		Rectangle r = new Rectangle(5,10);
		r.area();
		r.perimeter();
		Square s = new Square(10);
		s.area();
		s.perimeter();
		Square ss = new Square(5,5);
		ss.area();
		ss.perimeter();
// Array of Object creation
		Square[] squareArray = new Square[10];
//New object creation within the elements of Array
		Scanner sc = new Scanner(System.in);
		for (int i =0; i <=9; i++) {
	//user input of square length
			System.out.println("Enter the length of the square.");
			squareArray[i] = new Square(sc.nextInt());
	//calling method of object array
			squareArray[i].area();
			squareArray[i].perimeter();
		}
	}
}
