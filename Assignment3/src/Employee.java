
public class Employee extends Member{
	static String specialization = "Worker";
	
	public static void main(String[] args) {
		Member m = new Employee();
//Setting the value of parent variables with the help of getter and setter
		m.setName("Ram Sharma");
		m.setAge(21);
		m.setPhoneNumber(1234567890);
		m.setAddress("1234 asd Street");
		m.setSalary(5000);
//Printing the variables by accessing variables using Employee childclass.
		System.out.println("Name:" + m.name + " Age:" + m.age + " PhoneNo:" +  m.phoneNumber + " Address:" +  m.address + " Specialization:"+ specialization );
		m.printSalary();		
	}
}
