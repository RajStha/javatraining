
public class ParentClassPrivate {
	private void print1() {
		System.out.println("This is a parent class.");
	}	
	
	public static void main(String[] args) {
		ParentClassPrivate pc = new ParentClassPrivate();
//Calling method of parent class by object of parent class
		pc.print1();		
	}
}
