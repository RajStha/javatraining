
class DailyMessage {
	String getMessage(String day) {
		if(day.equals("Monday")||day.equals("Wednesday")||day.equals("Friday")) {
			return "study day";
		} else if(day.equals("Tuesday")||day.equals("Thursday")){
			return "gym day";
		} else if(day.equals("Saturday")||day.equals("Sunday")){
			return "weekend";
		} else {
			return "invalid day";
		}
	}
}