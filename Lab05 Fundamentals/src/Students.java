
public class Students {

	public static void main(String[] args) {
		String [] names1 = new String [3];
		names1[0] = "Ram";
		names1[1] = "Shyam";
		names1[2] = "Ghanashyam";
		System.out.println(names1[0]);
		System.out.println(names1[1]);
		System.out.println(names1[2]);
		String [] names2 = {"Gita", "Sita", "Rita"};
		if (names2.length == 3) {
			System.out.println("The array length is 3.");
		} else {
			System.out.println("The array length is not 3");
		}
		int i=0;
		System.out.println("The value of array is: ");
		for(i=0; i<names2.length; i++) {
			System.out.println(names2[i]);
		}
		
	}
}
