import java.util.Arrays;

public class TopThreeBuilding {

	public static void main(String[] args) {
		int temp = 0;
		int [] buildingHeights = {25,19,23,45,18,23,24,19};
//Arranging in descending order
		for(int i=0; i<buildingHeights.length; i++) {
			for(int j=i+1; j<buildingHeights.length; j++) {
				if (buildingHeights[i]<=buildingHeights[j]) {
					temp = buildingHeights[i];
					buildingHeights[i] = buildingHeights[j];
					buildingHeights[j] = temp;
				}
			}			
		}
		
//Printing Full array in string format
		System.out.println("Modified Array: " + Arrays.toString(buildingHeights));
//Printing top 3 buildings
		System.out.println("The top 3 buildings Heights are: ");
		for(int k=0; k<3; k++) {
			System.out.println(buildingHeights[k]);
		}
		
	}
}
