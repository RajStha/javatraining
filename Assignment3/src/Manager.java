
public class Manager extends Member{
	static String department = "Grocery";
	
	public static void main(String[] args) {
		Member m = new Manager();
//initializing the value of parent class variables from child class object
		m.setName("Shyam Pandit");
		m.setAge(30);
		m.setPhoneNumber(987654321);
		m.setAddress(" 567 Street qwe");
		m.setSalary(7000);
//Printing the variables by accessing variables using Employee child class.
		System.out.println("Name:" + m.name + " Age:" + m.age + " PhoneNo:" +  m.phoneNumber + " Address:" +  m.address + " Department:"+ department );
		m.printSalary();
	}
}
