
public class EqualString {
	void checkEqual(String s1, String s2) {
		if (s1.equals(s2)) {
			System.out.println(true);
		} else {
			System.out.println(false);
		}
	}
	public static void main(String[] args) {
		String str1 = "Stephen Edwin King";
		String str2 = "Walter Winchell";
		String str3 = "Nike Royko";
		String str4 = "Stephen Edwin King";
		
		EqualString es = new EqualString();
		es.checkEqual(str1, str2);
		es.checkEqual(str1, str3);
		es.checkEqual(str1, str4);
	}
}
