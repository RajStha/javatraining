
class CalculatorClient {

	public static void main(String[] args) {
		Calculator c = new Calculator();
		System.out.println("sum: " + c.add(1, 2));
		System.out.println("Difference: " + c.subtract(2.2, 1.2));
		System.out.println("Product: " + c.multiply(3, 4));
		System.out.println("Divison: " + c.divide(7, 8));
		System.out.println("String Value: " + c.findStringLength("Welcome here"));
		System.out.println("First Number is greater: " + c.compare(5, 6));
	}
}
