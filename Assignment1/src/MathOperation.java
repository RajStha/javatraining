import java.util.Scanner;

public class MathOperation {
	void calculateMath(int n1, int n2) {
		int max=0;
		int min=0;
		System.out.println("Sum of two integers: " + (n1+n2));
		System.out.println("Difference of two integers: " + (n1-n2));
		System.out.println("Product of two intgers: " + (n1*n2));
		System.out.println("Average of two integers: " + ((double) ((n1+n2)/2)));
		System.out.println("Distance of two integers: " + (n1-n2));
		if (n1>n2) {
			max = n1;
			min = n2;
		} else {
			max = n2;
			min = n1;
		}
		System.out.println("Max integer: " + max);
		System.out.println("Min integer: " + min);
		
	}
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter first input integer");
		int number1 = s.nextInt();
		System.out.println("Enter second input integer");
		int number2 = s.nextInt();
		MathOperation m = new MathOperation();
		m.calculateMath(number1, number2);
		
				
	}
}
