
public class CharReplace {
	String replaceChar(String x, int pos) {
		String returnable = x.substring(0,pos)+x.substring(pos+1);
		return (returnable);
	}
	
	public static void main(String[] args) {
		CharReplace cr = new CharReplace();
		System.out.println(cr.replaceChar("kitten", 1));
		System.out.println(cr.replaceChar("kitten", 0));
		System.out.println(cr.replaceChar("kitten", 4));
		System.out.println(cr.replaceChar("kitten", 1));
	}
}


