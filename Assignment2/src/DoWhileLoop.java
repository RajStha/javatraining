import java.util.*;
public class DoWhileLoop {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		boolean repeat = true;
		do {
			System.out.println("Enter the first integer: ");
			int n1 = s.nextInt();
			System.out.println("Enter the second integer");
			int n2 = s.nextInt();
			int sum = n1 + n2;
			System.out.println("The sum of two given numbers are: " + sum);
			System.out.println("Enter true to perform new addition and false to exit");
			repeat = s.nextBoolean();
		} while(repeat);
	}
}
