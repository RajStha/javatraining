import java.util.Scanner;

public class ArrayCompare {
	boolean arrayCheck(int[] array1, int[] array2) {
		if ((array1[0] == array2 [0]) && (array1[array1.length-1] == array2[array2.length-1])) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
//Input array1 length and array element	
		int array1[ ] = {50,-20,0,30,40,60,12};
		int array2[] = {45,20,10,20,30,50,11};
		Scanner s = new Scanner(System.in);
		System.out.println("Enter number of elements of First Array: ");
		int n = s.nextInt();
		int a[] = new int[n];
	    System.out.println("Enter all the elements:");
        for(int i = 0; i < n; i++) {
        	a[i] = s.nextInt();
        }

      //Print out elements of array
        System.out.println("Array1: ");
        for(int k = 0; k < n; k++ ) {
        	System.out.println(a[k]);
        }
        
      //Input array2 length and array element
        System.out.println("Enter number of elements of second Array: ");
		int m = s.nextInt();
		int b[] = new int[m];
        System.out.println("Enter all the elements:");
        for(int i = 0; i < m; i++) {
        	b[i] = s.nextInt();
        }
        
      //Print out elements of array
        System.out.println("Array2: ");
        for(int j = 0; j < m; j++ ) {
        	System.out.println(b[j]);
        }
       
        ArrayCompare ac = new ArrayCompare();
        System.out.println("The result is: " + ac.arrayCheck(array1, array2));
        System.out.println("Yielded result: " + ac.arrayCheck(a, b));
	
	}


}
