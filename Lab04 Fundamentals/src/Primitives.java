
class Primitives {
	static void printInfo(int x, float y, char z, boolean b, String str){
		System.out.println("The value of integer is: " + x);
		System.out.println("The value of float is: " + y);
		System.out.println("The value of boolean is: " + b);
		System.out.println("The value of string is:" + str);
	}
	public static void main(String[] args) {
		int x = 1;
		float y = 1.1f;
		char z = 'a';
		boolean b = true;
		String str = "Welcome";
		printInfo(x, y, z, b, str);
}
}
