
public class ArrayElementCheck {
	
	boolean checkElement(int[] originalArray) {
		boolean result = true;
		int i=0;
		for(i=0; i<originalArray.length; i++) {
			if (originalArray[i] == 4 || originalArray[i] == 7) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}	

	public static void main(String[] args) {
		int originalArray[] = {5,7};
		ArrayElementCheck aec = new ArrayElementCheck();
		System.out.println("Array contains 4 or 7: " + aec.checkElement(originalArray));
	}
}
