
public class Simplification {
	void simplify(int problem){
		System.out.println(problem);
	}
	
	public static void main(String[] args) {
		Simplification s = new Simplification();
		s.simplify(-5+8*6);
		s.simplify((55+9)%9);
		s.simplify(20+-3*5/8);
		s.simplify(5+15/3*2-8%3);
	}

}
