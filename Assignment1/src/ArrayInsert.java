import java.util.*;

public class ArrayInsert {
	
	public static void main(String[] args) {
//Input array
		Scanner s = new Scanner(System.in);
		System.out.println("Enter number of elements of First Array: ");
		int n = s.nextInt();
		int userArray[] = new int[n];
		System.out.println("Enter all the elements:");
	    for(int i = 0; i < n; i++) {
	    	userArray[i] = s.nextInt();
	    }
	    System.out.println("Output Array: ");
		
		for(int k=0;k<userArray.length; k++) {			
			System.out.println(userArray[k]);
		}	
	    System.out.println("Enter number to replace the element: ");
	    int userNumber = s.nextInt();	
	    ArrayInsert i = new ArrayInsert();
	    System.out.println("Enter the index to replace an element: ");
	    int userIndex = s.nextInt();
	    userArray[userIndex] = userNumber;
	    System.out.println("Output Array: ");
		
		for(int k=0;k<userArray.length; k++) {			
			System.out.println(userArray[k]);
		}	  
	}
}
