
public class EmptyString {
	
	boolean isEmpty(String str) {
		if(str.length() == 0) {
			return true;
		} else {
			return false;
		}
	}
	public static void main(String[] args) {
		EmptyString es = new EmptyString();
		System.out.println(es.isEmpty(""));
		System.out.println(es.isEmpty(" "));
		System.out.println(es.isEmpty("a"));
}
}
