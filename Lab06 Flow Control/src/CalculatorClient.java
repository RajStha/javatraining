import java.util.*;

class CalculatorClient {

	public static void main(String[] args) {
		int [] userArray = {1,2,3,4,5};
		Scanner s = new Scanner(System.in);
//Input User Array
		System.out.println("Enter number of elements of First Array: ");
		int n = s.nextInt();
		int userArray2[] = new int[n];
	    System.out.println("Enter all the elements:");
        for(int i = 0; i < n; i++) {
        	userArray2[i] = s.nextInt();
        }
//Adding Array Support to the Calculator
        System.out.println("Enter the number to find in array.");
        int searchValue = s.nextInt();
        
		Calculator c = new Calculator();
		System.out.println("Total sum of Array: " + c.sumNumbers(userArray));
		System.out.println("Total sum of Array: "+c.sumNumbers(userArray2));
		System.out.println(searchValue + " appears in " + c.countValues(userArray2, searchValue) + " places in array.");
	}
}
