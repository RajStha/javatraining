
class Event {
//method that takes nothing and returns nothing.
	void method1() {
		System.out.println("Method that takes nothing and receives nothing.");
	}
//method that takes parameter and returns nothing.
	void method2(int x) {
		System.out.println("The value of integer \"x\" is: " + x);
	}
//method that takes parameter and returns value.
	int method3(int y, int z) {
		int sum = y + z;
		return sum;
	}
}
