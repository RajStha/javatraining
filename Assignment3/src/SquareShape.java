
public class SquareShape extends RectangleShape{
	void print4() {
		System.out.println("Square is a rectangle.");
	}
	
	public static void main(String[] args) {
		SquareShape s = new SquareShape();
		s.print4();
//Calling parent class from subclass of a subclass
		s.print1(); //shape print
//Calling parent class from a child class
		s.print2(); //rectangle print
		
	}
}
