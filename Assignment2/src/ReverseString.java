public class ReverseString {
	static void reverseString(String str) {
		String temp = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			temp = temp + str.charAt(i);
		}
		System.out.println(temp);
	}

	public static void main(String[] args) {
		reverseString("Hello World");
		reverseString("The quick brown fox.");
		reverseString("Edabit is really helpful!");
	}
}
