
public class ChildClassPrivate extends ParentClassPrivate{
	void print() {
		System.out.println("This is a child class.");
	}
	
	public static void main(String[] args) {
		ChildClassPrivate cc = new ChildClassPrivate();
//Calling method of ChildClass by object of child class.
		cc.print();
//Calling method of parent class by object of child class.
//		ParentClassPrivate p = new ChildClassPrivate();
//		p.print1();
	}
}
