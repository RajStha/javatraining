import java.util.*;
public class SalesRevenue {

	void getSalesInfo(int a, int b) {
		if (b<100) {
			System.out.println("The revenue from sale: " + (b*a));
			System.out.println("Discount: None");			
		} else if(100<=b && b<=120) {
			System.out.println("The revenue from sale: " + (b*a*0.9));
			System.out.println("Discount: " + (0.1*b*a));			
		} else {
			System.out.println("The revenue from sale: " + (b*a*0.85));
			System.out.println("Discount: " + (0.15*b*a));
		}
	}
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter unit price: ");
		int unitPrice = s.nextInt();
		System.out.println("Enter quantity: ");
		int quantity = s.nextInt();
		
		SalesRevenue sr = new SalesRevenue();
		sr.getSalesInfo(unitPrice, quantity);
	}
}
