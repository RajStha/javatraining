
public class ParentClass{
	int value = 10;
	void print1() {
		System.out.println("This is a parent class.");
	}
	
	
	public static void main(String[] args) {
		ParentClass pc = new ParentClass();
//Calling method of parent class by object of parent class
		pc.print1();		
	}
}


