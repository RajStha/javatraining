import java.util.*;
public class SpeedingTicket {
	
	static int caughtSpeeding(int s, boolean b) {
		if(s<=60 && !b) {
			return 0;
		}else if (61<=s && s<=80 && !b) {
			return 1;			
		}else if(s<=81 && !b) {
			return 2;
		}else {
			if(s<=65 && b) {
				return 0;
			}else if(65<s && s<=85) {
				return 1;
			}else {
				return 2;
			}
		}
	}
	public static void main(String[] args) {				
		System.out.println(caughtSpeeding(60,false));
		System.out.println(caughtSpeeding(65,false));
		System.out.println(caughtSpeeding(65,true));
		Scanner s = new Scanner(System.in);
		System.out.println("Enter your speed:");
		int userSpeed = s.nextInt();
		System.out.println("Is it your birthday today?");
		boolean isBirthday = s.nextBoolean();
		System.out.println(caughtSpeeding(userSpeed, isBirthday));
	}
}
