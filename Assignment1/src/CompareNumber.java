import java.util.Scanner;

public class CompareNumber {
	
	void notEqual(int n1, int n2) {
		boolean result;
		if (n1 != n2) {
			result = true;
		} else {
			result = false;
		}
		System.out.println("Not Equalt to: " + result);
	}
	
	void lessThan(int n1, int n2) {
		boolean result;
		if (n1 < n2) {
			result = true;
		} else {
			result = false;
		}
		System.out.println("Less Than: " + result);
		
	}
	
	void lessOrEqualTo(int n1, int n2) {
		boolean result;
		if (n1<=n2) {
			result = true;
		} else {
			result = false;
		}	
		System.out.println("Greater or Equal to: " + result);
	}	
	
	public static void main(String[] args) {
		
		int n1;
		int n2;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a First Number: ");
		n1 = in.nextInt();
	    System.out.println("Input first Integer: "+n1);
	    System.out.println("Enter a second Number: ");
		n2 = in.nextInt();
	    System.out.println("Input Second Integer: "+n2);
	    
	    CompareNumber c = new CompareNumber();
	    c.notEqual(n1,n2);
	    c.lessThan(n1, n2);
	    c.lessOrEqualTo(n1,n2);
	}
}
