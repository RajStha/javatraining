import java.util.*;

public class InterestMonthly {

	public static void main(String[] args) {
		int n;
		double P = 100000;
		double R = 0.04;
		Scanner s = new Scanner(System.in);	
		do {
			System.out.println("Enter the number of months between 0 and 100 inclusive.");
			n = s.nextInt();
			for (int i=0; i<n; i++) {
				double interest = (P*n/12*R);
				System.out.println(interest);
				P = (Math.ceil(( P + interest)/1000))*1000;
				System.out.println(P);
			}
		} while(n>101 );					
	}
}
