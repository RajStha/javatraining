import java.util.*;
public class EqualNumbers {
	static void areEqual(int a, int b, int c, int d) {
		if (a==b && b==c && c==d) {
			System.out.println("Numbers are equal!");
		} else {
			System.out.println("Numbers are not equal!");
		}
	}
	public static void main(String[] args) {
		areEqual(25,37, 45, 23);
		Scanner s = new Scanner(System.in);
		System.out.println("Enter your four numbers respectively");
		int firstNumber = s.nextInt();
		int secondNumber = s.nextInt();
		int thirdNumber = s.nextInt();
		int fourthNumber = s.nextInt();
		areEqual(firstNumber, secondNumber, thirdNumber, fourthNumber);
	}
}
