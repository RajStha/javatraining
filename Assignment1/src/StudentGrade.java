
public class StudentGrade {

	char getGrades(double a) {
		if (a>=90) {
			return 'A';
		} else if(a>=70 && a<90) {
			return 'B';			
		}else if (a>=50 && a<70) {
			return 'C';
		}else {
			return 'F';
		}
	}
	public static void main(String[] args) {
		int quizScore = 80;
		int midTermScore = 68;
		int finalScore = 90;
		double averageScore = (quizScore+midTermScore+finalScore)/3;
		StudentGrade sg = new StudentGrade();
		System.out.println("Your grade is "+ sg.getGrades(averageScore) +".");
	}
}
