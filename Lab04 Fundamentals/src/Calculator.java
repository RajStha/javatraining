
class Calculator {
//methods to take two numbers and result
	int add(int a, int b) {
		int sum = a + b;
		return sum;
	}
	
	double subtract(double c, double d) {
		double difference = c - d;
		return difference;
	}
	
	int multiply(int e, int f) {
		int product = e*f;
		return product;
	}
	
	double divide(int g, int h) {
		double division = g/h;
		return division;
	}
	
	int findStringLength(String i) {
		int length = i.length();
		return length;
	}
	
	boolean compare(int j, int k) {
		boolean result;
		if (j>k) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}
	
	int sumNumbers(int [] arr1) {
		int sum = 0;
		for (int i=0; i<=arr1.length-1; i++) {
			sum += arr1[i];
		}
		return sum;
	}
	
	int countValues(int []arr, int a) {
		int count = 0;
		for (int i=0; i<=arr.length-1; i++) {
			if (arr[i]==a) {
				count++;
			}
		}
		if (count>=5) {
			return 5; 
		} else {
			return count;
		}
	}
}
